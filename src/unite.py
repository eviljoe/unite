#!/usr/bin/env python3


import argparse
from argparse import Namespace
from typing import Union

from jnscommons import jnsstr
from jnscommons import jnsvalid

from common import uniteutils
from common.options import UniteOptions


def main():
    opts = _parse_args()
    _validate_opts(opts)
    _archive(opts)


def _parse_args() -> Union[UniteOptions, Namespace]:
    parser = argparse.ArgumentParser(description='Archive in different formats', epilog=_create_help_epilog(),
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    for archiver in uniteutils.ARCHIVERS:
        ext = archiver.get_file_extensions()[0]
        parser.add_argument(f'--{ext.replace(".", "-")}', action='store_const', const=ext, dest='format',
                            help=f'Add files to a {ext} archive')

    parser.add_argument('--dry-run', action='store_true', default=False, dest='dry_run',
                        help='Output what actions will be performed without taking them (default: %(default)s)')
    parser.add_argument('--verbose', action='store_true', default=False, dest='verbose',
                        help='Print more information about what actions are being taken (default: %(default)s)')
    parser.add_argument('dest', metavar='dest_archive', default='',
                        help='The name of the archive to be created')
    parser.add_argument('files', nargs='*', metavar='file', default=[],
                        help='The files or directories to be added to the archive')

    return _to_options(parser.parse_args())


def _to_options(ns: Namespace) -> UniteOptions:
    return UniteOptions(
        dest=ns.dest,
        dry_run=ns.dry_run,
        files=ns.files,
        format=ns.format,
        verbose=ns.verbose,
    )


def _create_help_epilog() -> str:
    return jnsstr.wrap_str_array([
        'The archiving format will be determined by first checking to see if a format is manually specified.  If more '
        'than one format is manually specified, the last one specified will be used.  If no format is specified, it '
        'will be determined by checking the specified archive\'s file extension.  If the format cannot be determined '
        'from the file extension, an error will be thrown.'
    ])


def _validate_opts(opts: UniteOptions) -> None:
    jnsvalid.validate_all_exist(opts.files)


def _archive(opts: UniteOptions) -> None:
    uniteutils.get_archiver(opts.format, opts.dest).create_archive(opts)


if __name__ == '__main__':
    main()
