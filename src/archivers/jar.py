import shutil
import textwrap

from common.options import UniteOptions, DisbandOptions
from . import abstractarchiver


class JARArchiver(abstractarchiver.AbstractArchiver):
    def get_file_extensions(self) -> [str]:
        return ['jar', 'war', 'ear']

    def list_contents(self, opts: DisbandOptions) -> int:
        print('\n/* *********** */\n/* MANIFEST.MF */\n/* *********** */\n')
        exit_code = self.run_cmd(opts, ['unzip', '-p', opts.archive, 'META-INF/MANIFEST.MF'])

        if exit_code == 0:
            print('/* ***** */\n/* FILES */\n/* ***** */\n')
            exit_code = self.run_cmd(opts, ['jar', 'tvf', opts.archive])

        return exit_code

    def extract_archive(self, opts: DisbandOptions) -> int:
        return self.run_cmd(opts, ['unzip', opts.archive])

    def create_archive(self, _opts: UniteOptions) -> int:
        print(textwrap.fill(
            'Yeah... your expectations for this script were a bit high.  Use a dedicated tool for that.',
            width=shutil.get_terminal_size().columns))
        return 0
