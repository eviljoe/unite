import abc
import shlex
import subprocess
from abc import ABCMeta
from typing import Union

from common.options import UniteOptions, DisbandOptions


class AbstractArchiver(metaclass=ABCMeta):
    @abc.abstractmethod
    def get_file_extensions(self) -> [str]:
        raise NotImplementedError()

    @abc.abstractmethod
    def list_contents(self, opts: DisbandOptions) -> int:
        raise NotImplementedError()

    @abc.abstractmethod
    def extract_archive(self, opts: DisbandOptions) -> int:
        raise NotImplementedError()

    @abc.abstractmethod
    def create_archive(self, opts: UniteOptions) -> int:
        raise NotImplementedError()

    @staticmethod
    def run_cmd(opts: Union[UniteOptions, DisbandOptions], cmd: [str], cwd: str = '.') -> int:
        exit_code = 0

        if opts.verbose:
            print(' '.join([shlex.quote(token) for token in cmd]), flush=True)

        if not opts.dry_run:
            popen = subprocess.Popen(cmd, cwd=cwd)
            popen.wait()
            exit_code = popen.poll()

        return exit_code
