from common.options import UniteOptions, DisbandOptions
from . import abstractarchiver


class TARGZArchiver(abstractarchiver.AbstractArchiver):
    def get_file_extensions(self) -> [str]:
        return ['tar.gz', 'tgz']

    def list_contents(self, opts: DisbandOptions) -> int:
        return self.run_cmd(opts, ['tar', '--list', '--verbose', '--file', opts.archive, '--gzip'])

    def extract_archive(self, opts: DisbandOptions) -> int:
        return self.run_cmd(opts, ['tar', '--extract', '--verbose', '--file', opts.archive, '--gzip'])

    def create_archive(self, opts: UniteOptions) -> int:
        cmd = ['tar', '--create', '--verbose', '--file', opts.dest, '--gzip']
        cmd.extend(opts.files)

        return self.run_cmd(opts, cmd)
