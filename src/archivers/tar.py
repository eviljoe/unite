from common.options import UniteOptions, DisbandOptions
from . import abstractarchiver


class TARArchiver(abstractarchiver.AbstractArchiver):
    def get_file_extensions(self) -> [str]:
        return ['tar']

    def list_contents(self, opts: DisbandOptions) -> int:
        return self.run_cmd(opts, ['tar', '--list', '--verbose', '--file', opts.archive])

    def extract_archive(self, opts: DisbandOptions) -> int:
        return self.run_cmd(opts, ['tar', '--extract', '--verbose', '--file', opts.archive])

    def create_archive(self, opts: UniteOptions) -> int:
        cmd = ['tar', '--create', '--verbose', '--file', opts.dest]
        cmd.extend(opts.files)

        return self.run_cmd(opts, cmd)
