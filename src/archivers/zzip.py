from common.options import UniteOptions, DisbandOptions
from . import abstractarchiver


class ZipArchiver(abstractarchiver.AbstractArchiver):
    def get_file_extensions(self) -> [str]:
        return ['zip']

    def list_contents(self, opts: DisbandOptions) -> int:
        return self.run_cmd(opts, ['unzip', '-v', opts.archive])

    def extract_archive(self, opts: DisbandOptions) -> int:
        return self.run_cmd(opts, ['unzip', opts.archive])

    def create_archive(self, opts: UniteOptions) -> int:
        cmd = ['zip', '--verbose', '--recurse-paths', opts.dest]
        cmd.extend(opts.files)

        return self.run_cmd(opts, cmd)
