from dataclasses import dataclass


@dataclass
class UniteOptions:
    dest: str
    dry_run: bool
    files: [str]
    format: str
    verbose: bool


@dataclass
class DisbandOptions:
    archive: str
    dry_run: bool
    format: str
    list: bool
    verbose: bool
