#!/bin/bash

function _unite_complete() {
    local current="${COMP_WORDS[COMP_CWORD]}"
    local prev="${COMP_WORDS[COMP_CWORD-1]}"
    local dash_opts="\
        --dry-run \
        --help -h \
        --jar \
        --tar \
        --tar-bz2 \
        --tar-gz \
        --verbose \
        --zip \
    "

    COMPREPLY=( $(compgen -f -W "${dash_opts}" -- ${current} ) )
}

complete -F _unite_complete unite unite.py
