#!/bin/bash
# Run `disband.py'

set -euo pipefail

this_script="$(readlink -f "${BASH_SOURCE[0]}")"
this_script_dir="$(dirname "${this_script}")"
cd "${this_script_dir}/../src"

pipenv run ./disband.py "$@"
